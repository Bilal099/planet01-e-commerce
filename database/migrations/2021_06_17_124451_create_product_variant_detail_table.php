<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariantDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variant_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_variant_id')->references('id')->on('product_variant')->nullable();
            $table->integer('inventory')->nullable();
            // $table->integer('parent_id')->nullable();
            $table->foreignId('parent_id')->references('id')->on('product_variant_detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variant_detail');
    }
}
