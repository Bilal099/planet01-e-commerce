<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $email_encode = "";
    public $main_type = "";


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_encode,$main_type)
    {
        $this->email_encode = $email_encode;
        $this->main_type    = $main_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->main_type == "admin")
        {
            return $this->from(env('MAIL_USERNAME'))->view('admin.email.forgotPasswordMailTemplete')->with('email_decode',$this->email_encode);
        }
        elseif($this->main_type == "user")
        {
            return $this->from(env('MAIL_USERNAME'))->view('website.email.forgotPasswordMailTemplete')->with('email_decode',$this->email_encode);
        }
    }
}
